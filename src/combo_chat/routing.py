from channels.routing import include


routing = [
    include('chat.routing.websocket_routing', path=r"^/chat/$"),
    include('chat.routing.chat_routing'),
]
