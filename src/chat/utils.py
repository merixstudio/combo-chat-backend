from channels import Group


def get_room_group(room_id):
    return Group('room-{}'.format(room_id))
