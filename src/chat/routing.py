from channels.routing import route

from .consumers import (
    ws_connect,
    ws_disconnect,
    ws_receive,

    get_rooms,
    create_room,
    join_room,
    leave_room,
    create_message,
    change_username,
)


websocket_routing = [
    route('websocket.connect', ws_connect),
    route('websocket.disconnect', ws_disconnect),
    route('websocket.receive', ws_receive),
]

chat_routing = [
    route('chat.receive', get_rooms, command='^rooms.get$'),
    route('chat.receive', create_room, command='^rooms.create$'),
    route('chat.receive', join_room, command='^rooms.join$'),
    route('chat.receive', leave_room, command='^rooms.leave$'),
    route('chat.receive', create_message, command='^messages.create$'),
    route('chat.receive', change_username, command='^username.change$'),
]
