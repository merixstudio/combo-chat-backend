import json
import random

from channels import Channel, Group
from channels.sessions import (
    channel_session,
)

from .models import (
    Room,
)
from .utils import get_room_group


@channel_session
def ws_connect(message):
    username = 'Guest #{}'.format(random.randint(1, 1000))
    message.channel_session['rooms'] = []
    message.channel_session['username'] = username

    message.reply_channel.send({'accept': True})
    message.reply_channel.send({
        'text': json.dumps({
            'event': 'username.change',
            'payload': {
                'username': username,
            },
        })
    })
    Group('chat').add(message.reply_channel)


@channel_session
def ws_disconnect(message):
    Group('chat').discard(message.reply_channel)

    for room_id in message.channel_session['rooms']:
        get_room_group(room_id).discard(message.reply_channel)


@channel_session
def ws_receive(message):
    payload = json.loads(message['text'])
    payload['reply_channel'] = message.content['reply_channel']
    Channel('chat.receive').send(payload)


@channel_session
def get_rooms(message):
    message.reply_channel.send({
        'text': json.dumps({
            'event': 'rooms.get',
            'payload': {
                'rooms': list(Room.objects.values('name', 'id'))
            },
        })
    })


@channel_session
def create_room(message):
    payload = message['payload']
    room, created = Room.objects.get_or_create(name=payload['name'])

    Group('chat').send({
        'text': json.dumps({
            'event': 'rooms.create',
            'payload': {
                'id': room.pk,
                'name': room.name,
            },
        })
    })


@channel_session
def join_room(message):
    payload = message['payload']

    try:
        room = Room.objects.get(pk=payload['room'])
    except Room.DoesNotExist:
        return

    for room_id in message.channel_session['rooms']:
        get_room_group(room_id).discard(message.reply_channel)

    message.channel_session['rooms'] = [room.pk]
    room.websocket_group.add(message.reply_channel)

    message.reply_channel.send({
        'text': json.dumps({
            'event': 'rooms.join',
            'payload': {
                'id': room.pk,
                'messages': [
                    {
                        'room': room.pk,
                        'id': msg.id,
                        'text': msg.text,
                        'username': msg.username,
                        'created': str(msg.created),
                    }
                    for msg in room.messages.order_by('created')
                ],
            },
        })
    })


@channel_session
def leave_room(message):
    payload = message['payload']
    room = Room.objects.get(pk=payload['room'])
    room.websocket_group.discard(message.reply_channel)
    message.reply_channel.send({
        'text': json.dumps({
            'event': 'rooms.leave',
            'payload': {
                'id': room.pk,
            },
        })
    })


@channel_session
def create_message(message):
    payload = message['payload']
    room = Room.objects.get(pk=payload['room'])
    username = message.channel_session['username']
    message = room.messages.create(
        username=username,
        text=payload['text'],
    )

    room.websocket_group.send({
        'text': json.dumps({
            'event': 'messages.create',
            'payload': {
                'room': room.pk,
                'id': message.id,
                'text': message.text,
                'username': message.username,
                'created': str(message.created),
            },
        })
    })


@channel_session
def change_username(message):
    payload = message['payload']
    username = payload['username']

    message.channel_session['username'] = username
    message.reply_channel.send({
        'text': json.dumps({
            'event': 'username.change',
            'payload': {
                'username': username,
            },
        })
    })
