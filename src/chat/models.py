from django.db import models

from .utils import get_room_group


class Room(models.Model):

    name = models.CharField(
        max_length=100,
        unique=True,
    )

    @property
    def websocket_group(self):
        return get_room_group(self.pk)

    def __str__(self):
        return self.name


class Message(models.Model):

    room = models.ForeignKey(
        Room,
        related_name='messages',
        on_delete=models.CASCADE,
    )
    username = models.CharField(
        max_length=100,
    )
    text = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{} - {}'.format(self.room, self.username)
