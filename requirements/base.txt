asgi_redis==1.4.3
channels==1.1.8
django-cors-headers==2.1.0
django-environ==0.4.4
django-extensions==1.9.7
Django==1.11.7
psycopg2==2.7.3.2